# 实验2：用户及权限管理

- 学号：202010414427，姓名：赵云祥，班级：20软件工程4班

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

- 步骤1
 - 对于以下的对象名称con_res_role，sale，在实验的时候应该修改为自己的名称。
  
```
  Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> CREATE ROLE con_res_role;
GRANT connect,resource,CREATE VIEW TO con_res_role;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
--收回角色
REVOKE con_res_role FROM sale;
CREATE ROLE con_res_role
            *
第 1 行出现错误:
ORA-01921: 角色名 'CON_RES_ROLE' 与另一个用户名或角色名发生冲突


SQL> 
授权成功。

SQL> 
用户已创建。

SQL> 
用户已更改。

SQL> 
用户已更改。

SQL> 
授权成功。

SQL> SQL> 
撤销成功。

SQL> 

```

> 语句“ALTER USER sale QUOTA 50M ON users;”是指授权sale用户访问users表空间，空间限额是50M。

- 步骤2
  - 第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```
show user;
USER is "sale"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
SP2-0734: 未知的命令开头 "SQL> show ..." - 忽略了剩余的行。
SQL> SP2-0734: 未知的命令开头 "USER is "s..." - 忽略了剩余的行。
SQL> SP2-0042: 未知命令 "SQL>" - 其余行忽略。
SQL> 
PRIVILEGE
----------------------------------------
DROP ANY ANALYTIC VIEW
ALTER ANY ANALYTIC VIEW
CREATE ANY ANALYTIC VIEW
CREATE ANALYTIC VIEW
DROP ANY HIERARCHY
ALTER ANY HIERARCHY
CREATE ANY HIERARCHY
CREATE HIERARCHY
DROP ANY ATTRIBUTE DIMENSION
ALTER ANY ATTRIBUTE DIMENSION
CREATE ANY ATTRIBUTE DIMENSION

PRIVILEGE
----------------------------------------
CREATE ATTRIBUTE DIMENSION
READ ANY TABLE
ALTER ANY CUBE BUILD PROCESS
SELECT ANY CUBE BUILD PROCESS
ALTER ANY MEASURE FOLDER
SELECT ANY MEASURE FOLDER
EXEMPT DDL REDACTION POLICY
EXEMPT DML REDACTION POLICY
USE ANY JOB RESOURCE
LOGMINING
CREATE ANY CREDENTIAL

PRIVILEGE
----------------------------------------
CREATE CREDENTIAL
ALTER LOCKDOWN PROFILE
DROP LOCKDOWN PROFILE
CREATE LOCKDOWN PROFILE
SET CONTAINER
CREATE PLUGGABLE DATABASE
EXEMPT REDACTION POLICY
FLASHBACK ARCHIVE ADMINISTER
EM EXPRESS CONNECT
DROP ANY SQL TRANSLATION PROFILE
USE ANY SQL TRANSLATION PROFILE

PRIVILEGE
----------------------------------------
ALTER ANY SQL TRANSLATION PROFILE
CREATE ANY SQL TRANSLATION PROFILE
CREATE SQL TRANSLATION PROFILE
ADMINISTER SQL MANAGEMENT OBJECT
UPDATE ANY CUBE DIMENSION
UPDATE ANY CUBE BUILD PROCESS
DROP ANY CUBE BUILD PROCESS
CREATE ANY CUBE BUILD PROCESS
CREATE CUBE BUILD PROCESS
INSERT ANY MEASURE FOLDER
DROP ANY MEASURE FOLDER

PRIVILEGE
----------------------------------------
DELETE ANY MEASURE FOLDER
CREATE ANY MEASURE FOLDER
CREATE MEASURE FOLDER
UPDATE ANY CUBE
SELECT ANY CUBE
DROP ANY CUBE
CREATE ANY CUBE
ALTER ANY CUBE
CREATE CUBE
SELECT ANY CUBE DIMENSION
INSERT ANY CUBE DIMENSION

PRIVILEGE
----------------------------------------
DROP ANY CUBE DIMENSION
DELETE ANY CUBE DIMENSION
CREATE ANY CUBE DIMENSION
ALTER ANY CUBE DIMENSION
CREATE CUBE DIMENSION
COMMENT ANY MINING MODEL
ALTER ANY MINING MODEL
SELECT ANY MINING MODEL
DROP ANY MINING MODEL
CREATE ANY MINING MODEL
CREATE MINING MODEL

PRIVILEGE
----------------------------------------
EXECUTE ASSEMBLY
EXECUTE ANY ASSEMBLY
DROP ANY ASSEMBLY
ALTER ANY ASSEMBLY
CREATE ANY ASSEMBLY
CREATE ASSEMBLY
ALTER ANY EDITION
DROP ANY EDITION
CREATE ANY EDITION
CREATE EXTERNAL JOB
CHANGE NOTIFICATION

PRIVILEGE
----------------------------------------
READ ANY FILE GROUP
MANAGE ANY FILE GROUP
MANAGE FILE GROUP
CREATE ANY SQL PROFILE
ADMINISTER ANY SQL TUNING SET
ADMINISTER SQL TUNING SET
ALTER ANY SQL PROFILE
DROP ANY SQL PROFILE
SELECT ANY TRANSACTION
MANAGE SCHEDULER
EXECUTE ANY CLASS

PRIVILEGE
----------------------------------------
EXECUTE ANY PROGRAM
CREATE ANY JOB
CREATE JOB
ADVISOR
ANALYZE ANY DICTIONARY
EXECUTE ANY RULE
DROP ANY RULE
ALTER ANY RULE
CREATE ANY RULE
CREATE RULE
IMPORT FULL DATABASE

PRIVILEGE
----------------------------------------
EXPORT FULL DATABASE
EXECUTE ANY RULE SET
DROP ANY RULE SET
ALTER ANY RULE SET
CREATE ANY RULE SET
CREATE RULE SET
EXECUTE ANY EVALUATION CONTEXT
DROP ANY EVALUATION CONTEXT
ALTER ANY EVALUATION CONTEXT
CREATE ANY EVALUATION CONTEXT
CREATE EVALUATION CONTEXT

PRIVILEGE
----------------------------------------
GRANT ANY OBJECT PRIVILEGE
FLASHBACK ANY TABLE
DEBUG ANY PROCEDURE
DEBUG CONNECT ANY
DEBUG CONNECT SESSION
SELECT ANY DICTIONARY
RESUMABLE
ON COMMIT REFRESH
MERGE ANY VIEW
ADMINISTER DATABASE TRIGGER
ADMINISTER RESOURCE MANAGER

PRIVILEGE
----------------------------------------
DROP ANY OUTLINE
ALTER ANY OUTLINE
CREATE ANY OUTLINE
DROP ANY CONTEXT
CREATE ANY CONTEXT
DEQUEUE ANY QUEUE
ENQUEUE ANY QUEUE
MANAGE ANY QUEUE
DROP ANY DIMENSION
ALTER ANY DIMENSION
CREATE ANY DIMENSION

PRIVILEGE
----------------------------------------
CREATE DIMENSION
UNDER ANY TABLE
EXECUTE ANY INDEXTYPE
GLOBAL QUERY REWRITE
QUERY REWRITE
UNDER ANY VIEW
DROP ANY INDEXTYPE
ALTER ANY INDEXTYPE
CREATE ANY INDEXTYPE
CREATE INDEXTYPE
EXECUTE ANY OPERATOR

PRIVILEGE
----------------------------------------
DROP ANY OPERATOR
ALTER ANY OPERATOR
CREATE ANY OPERATOR
CREATE OPERATOR
EXECUTE ANY LIBRARY
DROP ANY LIBRARY
ALTER ANY LIBRARY
CREATE ANY LIBRARY
CREATE LIBRARY
UNDER ANY TYPE
EXECUTE ANY TYPE

PRIVILEGE
----------------------------------------
DROP ANY TYPE
ALTER ANY TYPE
CREATE ANY TYPE
CREATE TYPE
DROP ANY DIRECTORY
CREATE ANY DIRECTORY
DROP ANY MATERIALIZED VIEW
ALTER ANY MATERIALIZED VIEW
CREATE ANY MATERIALIZED VIEW
CREATE MATERIALIZED VIEW
GRANT ANY PRIVILEGE

PRIVILEGE
----------------------------------------
ANALYZE ANY
ALTER RESOURCE COST
DROP PROFILE
ALTER PROFILE
CREATE PROFILE
DROP ANY TRIGGER
ALTER ANY TRIGGER
CREATE ANY TRIGGER
CREATE TRIGGER
EXECUTE ANY PROCEDURE
DROP ANY PROCEDURE

PRIVILEGE
----------------------------------------
ALTER ANY PROCEDURE
CREATE ANY PROCEDURE
CREATE PROCEDURE
FORCE ANY TRANSACTION
FORCE TRANSACTION
ALTER DATABASE
AUDIT ANY
ALTER ANY ROLE
GRANT ANY ROLE
DROP ANY ROLE
CREATE ROLE

PRIVILEGE
----------------------------------------
DROP PUBLIC DATABASE LINK
CREATE PUBLIC DATABASE LINK
CREATE DATABASE LINK
SELECT ANY SEQUENCE
DROP ANY SEQUENCE
ALTER ANY SEQUENCE
CREATE ANY SEQUENCE
CREATE SEQUENCE
DROP ANY VIEW
CREATE ANY VIEW
CREATE VIEW

PRIVILEGE
----------------------------------------
DROP PUBLIC SYNONYM
CREATE PUBLIC SYNONYM
DROP ANY SYNONYM
CREATE ANY SYNONYM
CREATE SYNONYM
DROP ANY INDEX
ALTER ANY INDEX
CREATE ANY INDEX
DROP ANY CLUSTER
ALTER ANY CLUSTER
CREATE ANY CLUSTER

PRIVILEGE
----------------------------------------
CREATE CLUSTER
REDEFINE ANY TABLE
DELETE ANY TABLE
UPDATE ANY TABLE
INSERT ANY TABLE
SELECT ANY TABLE
COMMENT ANY TABLE
LOCK ANY TABLE
DROP ANY TABLE
BACKUP ANY TABLE
ALTER ANY TABLE

PRIVILEGE
----------------------------------------
CREATE ANY TABLE
CREATE TABLE
DROP ROLLBACK SEGMENT
ALTER ROLLBACK SEGMENT
CREATE ROLLBACK SEGMENT
DROP USER
ALTER USER
BECOME USER
CREATE USER
UNLIMITED TABLESPACE
DROP TABLESPACE

PRIVILEGE
----------------------------------------
MANAGE TABLESPACE
ALTER TABLESPACE
CREATE TABLESPACE
RESTRICTED SESSION
ALTER SESSION
CREATE SESSION
AUDIT SYSTEM
ALTER SYSTEM

已选择 239 行。

SQL> 
ROLE
--------------------------------------------------------------------------------
DBA
SELECT_CATALOG_ROLE
EXECUTE_CATALOG_ROLE
CAPTURE_ADMIN
EXP_FULL_DATABASE
IMP_FULL_DATABASE
AQ_ADMINISTRATOR_ROLE
DATAPUMP_EXP_FULL_DATABASE
DATAPUMP_IMP_FULL_DATABASE
GATHER_SYSTEM_STATISTICS
OPTIMIZER_PROCESSING_RATE

ROLE
--------------------------------------------------------------------------------
EM_EXPRESS_BASIC
EM_EXPRESS_ALL
SCHEDULER_ADMIN
HS_ADMIN_SELECT_ROLE
HS_ADMIN_EXECUTE_ROLE
XDBADMIN
XDB_SET_INVOKER
WM_ADMIN_ROLE
JAVA_ADMIN
JAVA_DEPLOY
OLAP_XS_ADMIN

ROLE
--------------------------------------------------------------------------------
OLAP_DBA
CON_RES_ROLE

已选择 24 行。

SQL> SP2-0042: 未知命令 "SQL>" - 其余行忽略。
SQL> 
表已创建。

SQL> 
已创建 1 行。

SQL> 
已创建 1 行。

SQL> 
视图已创建。

SQL> 
授权成功。

SQL> 
NAME
--------------------------------------------------
zhang
wang

```
- 步骤3
```
  SELECT * FROM sale.customers_view;
SELECT * FROM sale.customers_view
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在


SQL> SQL> SELECT * FROM sale.customers_view;
SP2-0734: 未知的命令开头 "SQL> SELEC..." - 忽略了剩余的行。
SQL>  SELECT * FROM sale.customers_view;
 SELECT * FROM sale.customers_view
                    *
第 1 行出现错误:
ORA-00942: 表或视图不存在

```
> 测试一下用户hr,sale之间的表的共享，只读共享和读写共享都测试一下。
sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

### 概要文件设置,用户最多登录时最多只能错误3次
```
  SQL> ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;

配置文件已更改


```
- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。

- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```
SQL> alter user sale  account unlock;

用户已更改。
```
### 数据库和表空间占用分析
> 当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
新用户sale使用默认表空间users存储表的数据。
随着用户往表中插入数据，表空间的磁盘使用量会增加。

### 查看数据库的使用情况
以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。
```
SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
USERS
/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
	 5 32767.9844 YES
SQL> SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
  2    3    4    5    6    7    8  
表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSAUX				      360    20.0625   339.9375      94.43
UNDOTBS1			      100    52.3125	47.6875      47.69
USERS					5     3.9375	 1.0625      21.25
SYSTEM				      250     5.4375   244.5625      97.83



```
- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

### 实验结束删除用户和角色
```
SQL> drop role con_res_role;
drop user sale cascade;

角色已删除。

SQL> 
用户已删除。

```

## 结论

- 创建用户和角色并授权：在实验中，我们创建了一个名为 sale 的用户，并为其创建了一个名为 con_res_role 的角色，然后将 connect、resource 和 create view 权限授予该角色，并将该角色授予给 sale 用户。这样，用户 sale 就能够使用角色 con_res_role 所包含的权限。
- 创建表和视图并授权：在实验中，我们创建了一个名为 customers 的表，并在其上插入了两条记录，然后创建了一个名为 customers_view 的视图，用于查询 customers 表的 name 字段。最后，我们将 select 权限授予 hr 用户，使其可以查询 customers_view 视图。
- 用户登录失败和账号锁定：在实验中，我们尝试使用错误的用户名和密码登录数据库，导致登录失败次数超过了数据库默认的最大失败次数（10次），从而导致 sale 用户的账号被锁定。我们可以使用 ALTER USER ... ACCOUNT UNLOCK 命令解锁该账号。
- 表空间管理：在实验中，我们使用了两个 SQL 查询语句来查询数据库中的表空间信息。第一个查询语句可以列出名为 USERS 的表空间所包含的数据文件信息，包括文件名、文件大小、最大文件大小以及是否可自动扩展。第二个查询语句可以列出所有表空间的信息，包括表空间名称、总大小、剩余大小、使用大小和使用率。
删除用户和角色：在实验结束时，我们使用 DROP USER 和 DROP ROLE 命令分别删除了 sale 用户和 con_res_role 角色以及与它们相关的所有对象。
- 综上，该实验涵盖了许多数据库用户和权限管理的方面，包括用户和角色的创建和授权、表和视图的创建和授权、账号登录失败和锁定、表空间管理以及用户和角色的删除。通过这个实验，我们可以学习到如何管理数据库用户和权限，以及如何维护和管理数据库对象。
