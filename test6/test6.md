<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

## 班级：软件工程2020级4班

## 学号：202010414427

## 姓名：赵云祥
- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。
## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 总体方案

#### 表空间设计方案：

表空间设计方案：  
- 表空间1：系统表空间（sale_sys）  
用于存储数据库的系统表和元数据信息。
大小根据实际需求进行设置，通常建议为较小的容量。  
- 表空间2：数据表空间（sale_data）  
用于存储用户数据表和索引。大小根据实际需求进行设置，需要足够的容量以容纳模拟数据量。  
表设计方案：  
1. 商品（Products）  
字段：商品ID（ProductID）、商品名称（ProductName）、商品描述（ProductDescription）、商品价格（Price）、库存数量（StockQuantity）。  
2. 客户（Customers）  
字段：客户ID（CustomerID）、客户姓名（CustomerName）、联系方式（Contact）、地址（Address）。
3. 订单（Orders）  
字段：订单ID（OrderID）、客户ID（CustomerID）、订单日期（OrderDate）、订单总额（TotalAmount）。  
4. 订单详情（OrderDetails）             
字段：订单详情ID（OrderDetailID）、订单ID（OrderID）、商品ID（ProductID）、数量（Quantity）、单价（UnitPrice）。

#### 权限与用户分配方案：
 
- 创建了两个用户：admin_user（管理员用户）和sales_user（销售员用户）。管理员用户被授予管理员角色，销售员用户被授予销售员角色。   
- 管理员角色（admin_role）被授予创建会话、创建表、创建序列和创建视图的权限。  
- 销售员角色（sales_role）被授予对商品、客户、订单和订单详情表的选择、插入、更新和删除权限。

#### 存储过程与函数设计：

- calculate_customer_total 存储过程用于计算指定客户的订单总额，calculate_order_total 存储过程用于计算指定订单的总额和商品数量，get_stock_quantity 函数用于获取指定商品的库存数量，is_order_paid 函数用于检查指定订单是否已付款。
#### 数据库备份方案：

- 使用 RMAN (Recovery Manager) 工具进行 Oracle 数据库备份和管理。

### 具体步骤

##### 第1步：创建用户,授予用户权限

```sql
--创建一个名为jackyar的用户，密码为123
GRANT USER jackyar IDENTIFIED BY 123;  
--授予jackyar权限
GRANT CONNECT,RESOURCE,CREATE SESSION TO jackyar;
```
![Alt text](../Oracle%20Final/oracle-2023-05-25-10-56-04.png)
##### 第2步：创建表空间

```sql
-- 创建系统表空间
CREATE TABLESPACE sale_sys
  DATAFILE 'sale_sys.dbf' SIZE 100M
  AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
  LOGGING
  EXTENT MANAGEMENT LOCAL AUTOALLOCATE
  SEGMENT SPACE MANAGEMENT AUTO;
```
- 创建了一个名为"sale_sys"的系统表空间。它指定了一个名为"sale_sys.dbf"的数据文件，初始大小为100MB，并且允许自动扩展，每次扩展100MB，最大大小为无限。此外，它启用了日志记录、本地自动分配的区管理以及自动段空间管理。

```sql
-- 创建数据表空间
CREATE TABLESPACE sale_data
  DATAFILE 'sale_data.dbf' SIZE 500M
  AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
  LOGGING
  EXTENT MANAGEMENT LOCAL AUTOALLOCATE
  SEGMENT SPACE MANAGEMENT AUTO;
```
- 创建了一个名为"sale_data"的数据表空间。它指定了一个名为"sale_data.dbf"的数据文件，初始大小为500MB，并且允许自动扩展，每次扩展100MB，最大大小为无限。与系统表空间相似，它也启用了日志记录、本地自动分配的区管理以及自动段空间管理。
![Alt text](oracle-2023-05-25-10-57-57.png)




##### 第3步：创建表


```sql
-- 创建商品表
CREATE TABLE products (
  product_id       NUMBER PRIMARY KEY,
  product_name     VARCHAR2(100),
  product_desc     VARCHAR2(500),
  price            NUMBER(10, 2),
  stock_quantity   NUMBER
)
TABLESPACE sale_data;
```
- 创建了一个名为"products"的表。它包含了一些列，包括商品ID（product_id）、商品名称（product_name）、商品描述（product_desc）、价格（price）和库存数量（stock_quantity）。商品ID被定义为主键。该表将存储在名为"sale_data"的表空间中。
```sql
-- 创建客户表
CREATE TABLE customers (
  customer_id      NUMBER PRIMARY KEY,
  customer_name    VARCHAR2(100),
  contact          VARCHAR2(100),
  address          VARCHAR2(500)
)
TABLESPACE sale_data;
```
- 创建了一个名为"customers"的表。它包含了一些列，包括客户ID（customer_id）、客户名称（customer_name）、联系方式（contact）和地址（address）。客户ID被定义为主键。该表将存储在名为"sale_data"的表空间中。
```sql
-- 创建订单表
CREATE TABLE orders (
  order_id         NUMBER PRIMARY KEY,
  customer_id      NUMBER,
  order_date       DATE,
  total_amount     NUMBER(10, 2),
  CONSTRAINT fk_orders_customers FOREIGN KEY (customer_id)
    REFERENCES customers (customer_id)
)
TABLESPACE sale_data;
```
- 创建了一个名为"orders"的表。它包含了一些列，包括订单ID（order_id）、客户ID（customer_id）、订单日期（order_date）和总金额（total_amount）。订单ID被定义为主键，并且还创建了一个外键约束，将订单表中的customer_id列与customers表中的customer_id列关联起来。该表将存储在名为"sale_data"的表空间中。
```sql
-- 创建订单详情表
CREATE TABLE order_details (
  order_detail_id  NUMBER PRIMARY KEY,
  order_id         NUMBER,
  product_id       NUMBER,
  quantity         NUMBER,
  unit_price       NUMBER(10, 2),
  CONSTRAINT fk_order_details_orders FOREIGN KEY (order_id)
    REFERENCES orders (order_id),
  CONSTRAINT fk_order_details_products FOREIGN KEY (product_id)
    REFERENCES products (product_id)
)
TABLESPACE sale_data;
```
- 创建了一个名为"order_details"的表。它包含了一些列，包括订单详情ID（order_detail_id）、订单ID（order_id）、商品ID（product_id）、数量（quantity）和单价（unit_price）。订单详情ID被定义为主键，并且还创建了两个外键约束，将订单详情表中的order_id列与orders表中的order_id列关联起来，将order_details表中的product_id列与products表中的product_id列关联起来。该表将存储在名为"sale_data"的表空间中。
![Alt text](oracle-2023-05-25-11-04-56.png)

##### 第4步插入模拟数据

```sql
-- 插入十万条商品数据
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO products (product_id, product_name, product_desc, price, stock_quantity)
VALUES (i, 'Product ' || i, 'Product description ' || i, 10.99, 100);
END LOOP;

COMMIT;
END;
/
-- 插入十万条客户数据
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO customers (customer_id, customer_name, contact, address)
VALUES (i, 'Customer ' || i, 'Contact ' || i, 'Address ' || i);
END LOOP;

COMMIT;
END;
/
-- 插入十万条订单数据
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO orders (order_id, customer_id, order_date, total_amount)
VALUES (i, i, SYSDATE, 100.00);
END LOOP;

COMMIT;
END;
/
-- 插入十万条订单详情数据
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO order_details (order_detail_id, order_id, product_id, quantity, unit_price)
    VALUES (i, i, i, 1, 10.99);
  END LOOP;
  
  COMMIT;
END;
/
```

![Alt text](oracle-2023-05-25-11-53-30.png)
![Alt text](oracle-2023-05-25-11-53-51.png)
![Alt text](oracle-2023-05-25-11-55-26.png)
![Alt text](oracle-2023-05-25-11-57-02.png)

##### 第5步：设计权限及用户分配方案。
```sql
-- 创建角色
CREATE ROLE admin_role;
CREATE ROLE sales_role;
```
创建两个角色一个管理员角色（admin_role），一个销售员角色（sales_role）
![](创建角色.png)
```sql
-- 授予管理员角色权限
GRANT CREATE SESSION TO admin_role;
GRANT CREATE TABLE TO admin_role;
GRANT CREATE SEQUENCE TO admin_role;
GRANT CREATE VIEW TO admin_role;

-- 授予销售员角色权限
GRANT CREATE SESSION TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO sales_role;
```
- 管理员角色（admin_role）被授予创建会话、创建表、创建序列和创建视图的权限。  
- 销售员角色（sales_role）被授予对商品、客户、订单和订单详情表的选择、插入、更新和删除权限。
- ![Alt text](oracle-2023-05-25-12-02-28.png)
![Alt text](oracle-2023-05-25-12-10-31.png)
##### 第6步：创建程序包
- 创建了一个名为 sales_pkg 的程序包。程序包中包含了四个子程序：两个存储过程和两个函数。
- (1) calculate_customer_total 存储过程接受一个客户ID作为输入参数，并通过该客户ID查询订单表，计算该客户的订单总额。计算结果存储在输出参数 p_total_amount 中。    
- (2)calculate_order_total 存储过程接受一个订单ID作为输入参数，并通过该订单ID查询订单详情表，计算该订单的总额和商品数量。计算结果分别存储在输出参数 p_total_amount 和 p_total_quantity 中。  
- (3)get_stock_quantity 函数接受一个产品ID作为输入参数，并通过该产品ID查询产品表，获取该产品的库存数量，并返回结果。  
- (4)is_order_paid 函数接受一个订单ID作为输入参数，并通过该订单ID查询订单表，检查订单是否已付款。如果订单的总金额大于0，则返回 TRUE，表示订单已付款；否则返回 FALSE，表示订单未付款。
```sql
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg IS
  -- 存储过程：根据客户ID计算客户的订单总额
  PROCEDURE calculate_customer_total(
    p_customer_id IN NUMBER,
    p_total_amount OUT NUMBER
  );

  -- 存储过程：根据订单ID计算订单的总额和商品数量
  PROCEDURE calculate_order_total(
    p_order_id IN NUMBER,
    p_total_amount OUT NUMBER,
    p_total_quantity OUT NUMBER
  );

  -- 函数：获取库存数量
  FUNCTION get_stock_quantity(
    p_product_id IN NUMBER
  ) RETURN NUMBER;

  -- 函数：检查订单是否已付款
  FUNCTION is_order_paid(
    p_order_id IN NUMBER
  ) RETURN BOOLEAN;
END sales_pkg;
/

-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY sales_pkg IS
  -- 存储过程：根据客户ID计算客户的订单总额
  PROCEDURE calculate_customer_total(
    p_customer_id IN NUMBER,
    p_total_amount OUT NUMBER
  ) IS
  BEGIN
    SELECT SUM(o.total_amount)
    INTO p_total_amount
    FROM orders o
    WHERE o.customer_id = p_customer_id;
  END calculate_customer_total;

  -- 存储过程：根据订单ID计算订单的总额和商品数量
  PROCEDURE calculate_order_total(
    p_order_id IN NUMBER,
    p_total_amount OUT NUMBER,
    p_total_quantity OUT NUMBER
  ) IS
  BEGIN
    SELECT SUM(od.unit_price * od.quantity), SUM(od.quantity)
    INTO p_total_amount, p_total_quantity
    FROM order_details od
    WHERE od.order_id = p_order_id;
  END calculate_order_total;

  -- 函数：获取库存数量
  FUNCTION get_stock_quantity(
    p_product_id IN NUMBER
  ) RETURN NUMBER IS
    l_stock_quantity NUMBER;
  BEGIN
    SELECT stock_quantity
    INTO l_stock_quantity
    FROM products
    WHERE product_id = p_product_id;

    RETURN l_stock_quantity;
  END get_stock_quantity;

  -- 函数：检查订单是否已付款
  FUNCTION is_order_paid(
    p_order_id IN NUMBER
  ) RETURN BOOLEAN IS
    l_total_amount NUMBER;
  BEGIN
    SELECT total_amount
    INTO l_total_amount
    FROM orders
    WHERE order_id = p_order_id;

    RETURN l_total_amount > 0;
  END is_order_paid;
END sales_pkg;
/

```
![Alt text](oracle-2023-05-25-12-21-27.png)

##### 调用存储过程
```sql
SET SERVEROUTPUT ON;
-- 调用存储过程：根据客户ID计算客户的订单总额
DECLARE
  p_customer_id NUMBER := 1; -- 替换为实际的客户ID
  p_total_amount NUMBER;
BEGIN
  sales_pkg.calculate_customer_total(p_customer_id, p_total_amount);
  DBMS_OUTPUT.PUT_LINE('Customer ID: ' || p_customer_id);
  DBMS_OUTPUT.PUT_LINE('Total Amount: ' || p_total_amount);
END;
/


-- 调用存储过程：根据订单ID计算订单的总额和商品数量
DECLARE
  p_order_id NUMBER := 1; -- 替换为实际的订单ID
  p_total_amount NUMBER;
  p_total_quantity NUMBER;
BEGIN
  sales_pkg.calculate_order_total(p_order_id, p_total_amount, p_total_quantity);
  DBMS_OUTPUT.PUT_LINE('Order ID: ' || p_order_id);
  DBMS_OUTPUT.PUT_LINE('Total Amount: ' || p_total_amount);
  DBMS_OUTPUT.PUT_LINE('Total Quantity: ' || p_total_quantity);
END;
/

-- 调用函数：获取库存数量
DECLARE
  p_product_id NUMBER := 1; -- 替换为实际的产品ID
  l_stock_quantity NUMBER;
BEGIN
  l_stock_quantity := sales_pkg.get_stock_quantity(p_product_id);
  DBMS_OUTPUT.PUT_LINE('Product ID: ' || p_product_id);
  DBMS_OUTPUT.PUT_LINE('Stock Quantity: ' || l_stock_quantity);
END;
/


-- 调用函数：检查订单是否已付款
DECLARE
  p_order_id NUMBER := 1; -- 替换为实际的订单ID
  l_order_paid BOOLEAN;
BEGIN
  l_order_paid := sales_pkg.is_order_paid(p_order_id);
  IF l_order_paid THEN
    DBMS_OUTPUT.PUT_LINE('Order ID: ' || p_order_id || ' is paid.');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Order ID: ' || p_order_id || ' is not paid.');
  END IF;
END;
/

```
![Alt text](oracle-2023-05-25-12-36-29.png)

##### 备份

```sql
-- 登录到数据库
sqlplus / as sysdba

-- 关闭数据库
SHUTDOWN IMMEDIATE

-- 启动数据库实例并挂载数据库
STARTUP MOUNT

-- 将数据库切换到归档日志模式
ALTER DATABASE ARCHIVELOG；

-- 打开数据库
ALTER DATABASE OPEN；
```
![Alt text](oracle-2023-05-25-12-40-01.png)
```sql
-- 使用 RMAN 工具进行备份
rman target /

-- 显示 RMAN 的当前配置和参数设置
SHOW ALL;

-- 执行完整数据库备份操作
BACKUP DATABASE;

-- 列出已备份的备份集信息
LIST BACKUP;
```
![Alt text](oracle-2023-05-25-12-53-12.png)
![Alt text](oracle-2023-05-25-12-53-31.png)
![Alt text](oracle-2023-05-25-12-53-45.png)
![Alt text](oracle-2023-05-25-12-53-52.png)

##### 实验总结
```- 在本次实验中，我们通过设计一个基于Oracle数据库的商品销售系统，进行了数据库设计、表空间创建、数据插入、存储过程和函数编写以及备份方案设计的实践。

- 首先，我们创建了四张表：products（商品表）、customers（客户表）、orders（订单表）和order_details（订单详情表），并使用两个表空间进行数据存储。通过合理的表结构设计和数据类型选择，确保了数据的完整性和准确性。

- 为了模拟真实的业务场景，我们插入了十万条虚拟数据，并对数据进行了平均分配，以保证数据的多样性和充足性，使系统能够处理大规模的销售业务。

- 为了支持复杂的业务逻辑，我们创建了一个程序包（sales_pkg），其中包含了计算客户订单总额、计算订单总额和商品数量、获取库存数量以及检查订单是否已付款等存储过程和函数。这些存储过程和函数提供了方便的接口，使系统能够快速进行各种业务计算和判断。

- 另外，我们设计了一个完全备份的数据库备份方案，包括创建备份表空间、执行完整数据库备份和备份集的列表显示。这样可以确保在发生意外情况时，能够恢复数据库到备份点，保障数据的安全性和可靠性。

- 通过本次实验，我们深入了解了Oracle数据库的设计和管理，掌握了数据库表的创建、存储过程和函数的编写，以及备份方案的设计和执行。这些技能对于构建稳定、高效的数据库系统非常关键，能够提高数据管理和业务处理的能力。```
